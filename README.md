# M2 Open API Proposal
Chris Yao 01/26/2021
##Introduction
M2 Open API provides clients a set of public consumable APIs.


##Sub-Projects/Micro Services
M2 Open API includes several sub projects such as 
1. m2o-admin-dashboard-front -- SPA front-end project for internal super admin usage.
2. m2o-admin-dashboard-back -- Express application hosting admin front-end application, and handle authentication and requests.
3. m2o-dashboard-front -- SPA front-end project for client usage
4. m2o-dashboard-back-back -- Express application hosting client front-end application, and handle authetication and requests.
5. m2o-core -- Open API core respond server cluster instance for worldwide client usage.
6. m2o-accounting -- Micro service for Add/Edit/Delete API request record.

##Database Design
![Database design](./screenshots/database-shortcut.png)
* m2o-admin -- Application administrator 
    * Create/Edit/Delete/Search company
    * Create/Edit/Delete/Search company user
    * Create/Edit/Delete/Search company authentication key
    * Create/Edit/Delete/Search authentication privilege types
    * Create/Edit/Delete/Search certain privilege instance of certain authentication key
    * Edit/Delete/Search request record
    
* m2o-client -- Application user
    * Create/Edit/Delete/Search company authentication key
    * Create/Edit/Delete/Search certain privilege instance of certain authentication key
    * Search request record
##Use Cases
M2 Open API has two major user groups which are Application Administrator and Client.
###
###